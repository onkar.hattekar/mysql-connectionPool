package com.afconn.core.business;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Reference;

import com.day.commons.datasource.poolservice.DataSourcePool;

public class ConnectionHelper {

	// create datasource in aem name it Customer
	// Configuration me na? kr dia hai wo
	// hmm to ye commit karke kar

	@Reference
	private static DataSourcePool source;

	// Returns a connection using the configured DataSourcePool
	public static Connection getConnection() {
		DataSource dataSource = null;
		Connection con = null;
		try {
			// Inject the DataSourcePool right here!
			dataSource = (DataSource) source.getDataSource("Customer");
			con = dataSource.getConnection();
			return con;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return con;
			
	}

	public static void close(Connection connection) {
		try {
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
