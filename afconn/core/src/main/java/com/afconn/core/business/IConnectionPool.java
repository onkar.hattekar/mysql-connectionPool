package com.afconn.core.business;
import java.sql.*;
public interface IConnectionPool {
	  public Connection getConnection();
}
