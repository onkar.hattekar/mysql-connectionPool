package com.afconn.core.business;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;

@Component(service=IConnectionPool.class)
public class ConnectionPoolImpl  implements IConnectionPool{

	 private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	  private static final String DB_URL = "jdbc:mysql://localhost:3306/cq";
	  private static final String USER = "root";
	  private static final String PASS = "root";
	  
	  @Activate
	  public void activate(){
	    try{
	    	DriverManager.registerDriver(new com.mysql.jdbc.Driver());
//	      Class.forName(JDBC_DRIVER);
	    }catch(Exception e){      
	      e.printStackTrace();
	    }
	  }
	  
	  public Connection getConnection(){
	    try {
			return DriverManager.getConnection(DB_URL,USER,PASS);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
		
	  }
	}

