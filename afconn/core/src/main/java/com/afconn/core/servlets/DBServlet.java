package com.afconn.core.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.afconn.core.business.IConnectionPool;



//@Component(service = Servlet.class, immediate=true, property = { "sling.servlet.methods=get", "sling.servlet.methods=post","sling.servlet.paths=/tekno/testing" })
@Component(service=Servlet.class,immediate=true,property = { "sling.servlet.methods=get", "sling.servlet.methods=post","sling.servlet.paths=/tekno/testing" })
public class DBServlet extends SlingAllMethodsServlet {
	@Reference
	IConnectionPool iConnectionPool;
	
	
	
	public void doGet(SlingHttpServletRequest request,SlingHttpServletResponse response) throws IOException
	{
		
		
		
		Logger logger=LoggerFactory.getLogger(this.getClass());
		try {			
			Connection conn = iConnectionPool.getConnection();
			
			response.getWriter().println("Connection: "+conn);
			
			 Statement statement = conn.createStatement();
			 ResultSet resultSet = statement.executeQuery("select * from customer");
			
			 while(resultSet.next()) {
				 response.getWriter().println(resultSet.getString("custFirst"));
			 }
			 
			
			
			logger.error("msg");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		
	}

}
